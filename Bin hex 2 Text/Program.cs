﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace bin_hex_2_Text
{
    class Program
    {
        static readonly byte[] HEX_BYTES = {  };

        static void Main(string[] args)
        {
            if (args.Length == 1 && args[0] == "-version")
            {
                Console.WriteLine(Application.ProductName + " " + Application.ProductVersion + " written by " + Application.CompanyName + " for catalinnc." + Environment.NewLine);
                Environment.Exit(0);
            }

            if (args.Length == 3)
            {
                string sourceFile = args[0];
                string targetFile = args[1];
                byte byteJump = Byte.Parse(args[2]);

                if (byteJump > 32)
                    byteJump = 32;

                if (File.Exists(targetFile))
                    File.Delete(targetFile);

                FileInfo fi = new FileInfo(sourceFile);
                
                if (fi.Length - 32 < 0)
                {
                    Console.WriteLine("Source file is too small.");
                    Console.ReadKey(true);
                    
                    Environment.Exit(0);
                }

                int bytesRead = 0;
                bool badBuffer = false;

                Stopwatch sw = new Stopwatch();
                sw.Start();
                byte[] contents = File.ReadAllBytes(sourceFile);
                using (Stream stream = new MemoryStream(contents))
                using (BinaryReader reader = new BinaryReader(stream))
                {
                    HashSet<byte[]> seenBuffer = new HashSet<byte[]>();
                    HashSet<string> seenKlic = new HashSet<string>();

                    byte[] buffer = new byte[32];
                    
                    while ((bytesRead = reader.Read(buffer, 0, buffer.Length)) > 31)
                    {
                        for (int i = 0; i < buffer.Length; i++)
                        {
                            if (buffer[i] != 0x30 && buffer[i] != 0x31 && buffer[i] != 0x32 && buffer[i] != 0x33 && buffer[i] != 0x34 && buffer[i] != 0x35 && buffer[i] != 0x36 && buffer[i] != 0x37 && buffer[i] != 0x38 && buffer[i] != 0x39 && // 0-9
                                buffer[i] != 0x41 && buffer[i] != 0x42 && buffer[i] != 0x43 && buffer[i] != 0x44 && buffer[i] != 0x45 && buffer[i] != 0x46 && // A-F
                                buffer[i] != 0x61 && buffer[i] != 0x62 && buffer[i] != 0x63 && buffer[i] != 0x64 && buffer[i] != 0x65 && buffer[i] != 0x66) // a-f
                            {
                                stream.Seek((1 + byteJump) - bytesRead, SeekOrigin.Current);
                                badBuffer = true;
                                break;
                            }

                            badBuffer = false;
                        }

                        if (!badBuffer)
                        {
                            string klic = Encoding.Default.GetString(buffer).ToUpper();

                            if (!seenKlic.Contains(klic))
                            {
                                seenKlic.Add(klic);
                                write_klic(klic, targetFile);
                            }

                            stream.Seek((1 + byteJump) - bytesRead, SeekOrigin.Current);
                        }
                    }
                    
                    stream.Seek(0, SeekOrigin.Begin);

                    buffer = new byte[64];

                    while ((bytesRead = reader.Read(buffer, 0, buffer.Length)) > 63)
                    {
                        for (int i = 0; i < buffer.Length; i++)
                        {
                            if (buffer[i] == 0 && buffer[i++] != 0)
                                continue;

                            if (i >= 64)
                                break;

                            if (buffer[i] != 0x30 && buffer[i] != 0x31 && buffer[i] != 0x32 && buffer[i] != 0x33 && buffer[i] != 0x34 && buffer[i] != 0x35 && buffer[i] != 0x36 && buffer[i] != 0x37 && buffer[i] != 0x38 && buffer[i] != 0x39 && // 0-9
                                buffer[i] != 0x41 && buffer[i] != 0x42 && buffer[i] != 0x43 && buffer[i] != 0x44 && buffer[i] != 0x45 && buffer[i] != 0x46 && // A-F
                                buffer[i] != 0x61 && buffer[i] != 0x62 && buffer[i] != 0x63 && buffer[i] != 0x64 && buffer[i] != 0x65 && buffer[i] != 0x66) // a-f
                            {
                                stream.Seek((1 + byteJump) - bytesRead, SeekOrigin.Current);
                                badBuffer = true;
                                break;
                            }

                            badBuffer = false;
                        }

                        if (!badBuffer)
                        {
                            List<byte> fixedBufferList = new List<byte>();

                            for (int i = 0; i < buffer.Length; i++)
                            {
                                if (buffer[i] != 0)
                                    fixedBufferList.Add(buffer[i]);
                            }

                            byte[] fixedBuffer = fixedBufferList.ToArray();
                            string klic = Encoding.Default.GetString(fixedBuffer).ToUpper();

                            if (!seenKlic.Contains(klic))
                            {
                                seenKlic.Add(klic);
                                write_klic(klic, targetFile);
                            }
                            
                            stream.Seek((1 + byteJump) - bytesRead, SeekOrigin.Current);
                        }
                    }
                }
                sw.Stop();

                Console.WriteLine("ELAPSED: " + sw.Elapsed);
                Console.ReadKey(true);

                Environment.Exit(0);
            }

            Console.WriteLine("Usage: sourcefile targetfile bytestoskip");
            Console.WriteLine("Example: eboot.elf klics.txt 0");
        }

        static void write_klic(string klic, string targetFile)
        {
            using (TextWriter writer = File.AppendText(targetFile))
                writer.WriteLine(klic);
        }
    }
}
